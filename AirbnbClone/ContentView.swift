//
//  ContentView.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 30/05/2024.
//

import SwiftUI

struct ContentView: View {
    
    private let authManager: AuthManager
    
    init(authManager: AuthManager) {
        self.authManager = authManager
    }
    
    var body: some View {
        MainTabView(authManager: authManager)
            .environment(\.colorScheme, .light)
    }
}

#Preview {
    ContentView(authManager: AuthManager(service: MockAuthService()))
}
