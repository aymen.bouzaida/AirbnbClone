//
//  AuthManager.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import Foundation

class AuthManager: ObservableObject{
    @Published var userSessionID: String?
    
    private let service: AuthServiceProtocol
    
    init(service: AuthServiceProtocol) {
        self.service = service
        self.userSessionID = "12312313"
    }
    
    @MainActor
    func login(withEmail email: String, password: String) async throws {
        self.userSessionID = try await service.login(withEmail: email, password: password)
    }
    
    @MainActor
    func createUser(WithEmail email: String, password: String, fullname: String) async throws {
        self.userSessionID = try await service.createUser(withEmail: email, password: password, fullname: fullname)
    }
    
    @MainActor
    func signout() {
        service.Signout()
        self.userSessionID = nil
    }
}
