//
//  AutheticationFormProtocol.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import Foundation

protocol AutheticationFormProtocol {
    var formIsValid: Bool { get }
}
