//
//  AuthServiceProtocol.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import Foundation

protocol AuthServiceProtocol {
    func login(withEmail email: String, password: String) async throws -> String?
    func createUser(withEmail email: String, password: String, fullname: String) async throws -> String?
    func Signout()
}
