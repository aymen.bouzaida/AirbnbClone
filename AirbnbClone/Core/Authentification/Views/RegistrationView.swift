//
//  RegistrationView.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import SwiftUI

struct RegistrationView: View {
    @State private var email = ""
    @State private var password = ""
    @State private var fullname = ""
    @StateObject var viewModel = RegistrationViewModel(service: MockAuthService())
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack {
            Spacer()
            
            Image(.airbnbAppIcon)
                .resizable()
                .scaledToFit()
                .frame(width: 120, height: 120)
                .padding()
            
            VStack {
                TextField("Enter your email", text: $email)
                    .modifier(PrimaryTextFieldModifier())
                
                SecureField("Enter your password", text: $password)
                    .modifier(PrimaryTextFieldModifier())
                
                TextField("Enter your fullname", text: $email)
                    .modifier(PrimaryTextFieldModifier())
            }
            
            Button(action: {
                Task { await viewModel.createUser(withemail: email, password: password, fullName: fullname)}
            }, label: {
                Text("Create Account")
                    .modifier(PrimaryButtonModifier())
            }) 
            .disabled(!formIsValid)
            .opacity(formIsValid ? 1.0 : 0.7)
            .padding(.vertical)
            
            Spacer()
            
            Divider()
            
            Button(action: {
                dismiss()
            }, label: {
                HStack(spacing: 2) {
                    Text("Already have an account?")
                    
                    Text("sigin")
                        .fontWeight(.semibold)
                }
            })
            .padding(.vertical)
        }
    }
}
// MARK: - AutheticationFormProtocol

extension RegistrationView : AutheticationFormProtocol {
    var formIsValid: Bool {
        return !email.isEmpty &&
        email.isValidEmail &&
        !password.isEmpty &&
        password.count > 5 &&
        !fullname.isEmpty
    }
}

#Preview {
    RegistrationView()
}
