//
//  MockAuthService.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import Foundation

struct MockAuthService: AuthServiceProtocol {
 
    func login(withEmail email: String, password: String) async throws -> String? {
        return NSUUID().uuidString
    }
    
    func createUser(withEmail email: String, password: String, fullname: String) async throws -> String? {
        return NSUUID().uuidString
    }
    
    func Signout() {}
}
