//
//  FirebaseAuthService.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import Firebase

struct FirebaseAuthService: AuthServiceProtocol {
 
    func login(withEmail email: String, password: String) async throws -> String? {
        do {
            let result = try await Auth.auth().signIn(withEmail: email, password: password)
            return result.user.uid
        } catch  {
            print("DEBUG: unable to auth via firebase with error: \(error)")
            throw error
        }
    }
    
    func createUser(withEmail email: String, password: String, fullname: String) async throws -> String? {
        do {
            let result = try await Auth.auth().createUser(withEmail: email, password: password)
            return result.user.uid
        } catch  {
            print("DEBUG: unable to auth via firebase with error: \(error)")
            throw error
        }
    }
    
    func Signout() {
        try? Auth.auth().signOut()
    }
}
