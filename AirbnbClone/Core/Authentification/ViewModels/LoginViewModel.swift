//
//  LoginViewModel.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import Foundation

class LoginViewModel: ObservableObject {
    
    private let authManager: AuthManager
    
    init(authManager: AuthManager) {
        self.authManager = authManager
    }
    
    func login(withemail email: String, password: String) async {
        do {
            try await authManager.login(withEmail: email, password: password)
        } catch {
            print("DEBUG: Failed to login with error: \(error)")
        }
    }
}
