//
//  RegistrationViewModel.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import Foundation

class RegistrationViewModel: ObservableObject {
    private let service: MockAuthService
    
    init(service: MockAuthService) {
        self.service = service
    }
    
    func createUser(withemail email: String, password: String, fullName: String) async {
        do {
            try await service.createUser(withEmail: email, password: password, fullname: fullName)
        } catch {
            print("DEBUG: Failed to create user with error: \(error)")
        }
    }
}
