//
//  ProfileView.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 03/06/2024.
//

import SwiftUI

struct ProfileView: View {
    @State private var showLogin = false
    @ObservedObject var authManager: AuthManager
    
    init(authManager: AuthManager) {
        self.authManager = authManager
    }

    var body: some View {
        VStack {
            if authManager.userSessionID == nil {
                // proffile login View
                ProfileLoginView(showLogin: $showLogin)
            } else {
                UserProfileHeaderView()
            }
            // profile options
            VStack(spacing: 24) {
                ProfileOptionRowView(imageName: "gear", title: "Settings")
                ProfileOptionRowView(imageName: "gear", title: "Accessibility")
                ProfileOptionRowView(imageName: "questionmark.circle", title: "Visit the help center")
            }
            .padding(.vertical)
            
            if authManager.userSessionID != nil {
                
                Button(action: {
                    authManager.signout()
                }, label: {
                    Text("Log Out")
                        .underline()
                        .foregroundStyle(.black)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                })
                .frame(maxWidth: .infinity, alignment: .leading)
               
            }
            
        }
        .sheet(isPresented: $showLogin, content: {
            LoginView(authManager: authManager)
        })
        .padding()
    }
}

#Preview {
    ProfileView(authManager: AuthManager(service: MockAuthService()))
}
