//
//  UserProfileHeaderView.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 08/06/2024.
//

import SwiftUI

struct UserProfileHeaderView: View {
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 28)
                .fill(.white)
                .frame(width: 300, height: 200)
                .shadow(radius: 10)
            VStack {
                CircularProfileImageView(size: .xLarge)
                
                VStack {
                    Text("Aymen Bouzaida")
                        .font(.headline)
                    
                    Text("Guest")
                        .font(.footnote)
                }
            }
        }
    }
}

#Preview {
    UserProfileHeaderView()
}
