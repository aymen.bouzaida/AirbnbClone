//
//  WishlistsView.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 03/06/2024.
//

import SwiftUI

struct WishlistsView: View {
    @State private var showLogin = false
    
    @ObservedObject var authManager: AuthManager
    
    init(authManager: AuthManager) {
        self.authManager = authManager
    }
    
    var body: some View {
        NavigationStack {
            VStack {
                if authManager.userSessionID == nil {
                   WishlistLoginView(showLogin: $showLogin)
                } else {
                    WishlistEmptyStateView()
                }
            }
            .padding()
            .navigationTitle("Wishlists")
            .sheet(isPresented: $showLogin, content: {
                LoginView(authManager: authManager)
            })
        }
       
    }
}

#Preview {
    WishlistsView(authManager: AuthManager(service: MockAuthService()))
}
