//
//  DeveloperPreview.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 03/06/2024.
//

import Foundation

class DeveloperPreview {
    static let shared = DeveloperPreview()
    
    var listings: [Listing] = [
        .init(
            id: NSUUID().uuidString,
            ownerId: NSUUID().uuidString,
            ownerName: "Aymen Bouzaida",
            ownerImageUrl: "male-profile-photo",
            numberOfBedrooms: 4,
            numberOfBeds: 3,
            numberOfGuests: 4,
            numberOfBathrooms: 4,
            pricePerNight: 567,
            latitude: 25.7850,
            longitude: -80.1936,
            imageUrls: ["listing-2", "listing-1", "listing-3", "listing-4"],
            address: "124 Main St",
            city: "Miami",
            state: "Florida",
            title: "Miami Villa",
            rating: 4.32,
            features: [.selfCheckIn, .superHost],
            ameneties: [.wifi, .alarmSystem, .balcony, .laundry, .tv],
            type: .villa
        ),
        .init(
            id: NSUUID().uuidString,
            ownerId: NSUUID().uuidString,
            ownerName: "Aymen Bouzaida",
            ownerImageUrl: "male-profile-photo",
            numberOfBedrooms: 4,
            numberOfBeds: 3,
            numberOfGuests: 4,
            numberOfBathrooms: 4,
            pricePerNight: 567,
            latitude: 25.7706,
            longitude: -80.1340,
            imageUrls: ["listing-3", "listing-2", "listing-1", "listing-4"],
            address: "124 Main St",
            city: "Miami",
            state: "Florida",
            title: "Miami Beach House",
            rating: 4.32,
            features: [.selfCheckIn, .superHost],
            ameneties: [.wifi, .alarmSystem, .tv, .kitchen, .office],
            type: .house
        ),
        .init(
            id: NSUUID().uuidString,
            ownerId: NSUUID().uuidString,
            ownerName: "Steve Johnson",
            ownerImageUrl: "male-profile-photo",
            numberOfBedrooms: 4,
            numberOfBeds: 3,
            numberOfGuests: 4,
            numberOfBathrooms: 4,
            pricePerNight: 763,
            latitude: 25.7650,
            longitude: -80.1936,
            imageUrls: ["listing-4", "listing-2", "listing-3", "listing-1"],
            address: "124 Main St",
            city: "Miami",
            state: "Florida",
            title: "Beatiful Miami apartment in downtown Brickell",
            rating: 4.9,
            features: [.selfCheckIn, .superHost],
            ameneties: [.wifi, .alarmSystem, .balcony],
            type: .apartment
        ),
        .init(
            id: NSUUID().uuidString,
            ownerId: NSUUID().uuidString,
            ownerName: "Harry Styles",
            ownerImageUrl: "male-profile-photo",
            numberOfBedrooms: 4,
            numberOfBeds: 3,
            numberOfGuests: 4,
            numberOfBathrooms: 4,
            pricePerNight: 763,
            latitude: 34.2,
            longitude: -118.0426,
            imageUrls: ["listing-7", "listing-8", "listing-5", "listing-1"],
            address: "124 Main St",
            city: "Los Angeles",
            state: "California",
            title: "Beatiful Los Angeles home in Malibu",
            rating: 4.2,
            features: [.selfCheckIn, .superHost],
            ameneties: [.wifi, .alarmSystem, .pool],
            type: .apartment
        ),
        .init(
            id: NSUUID().uuidString,
            ownerId: NSUUID().uuidString,
            ownerName: "Timothy Chalamet",
            ownerImageUrl: "male-profile-photo",
            numberOfBedrooms: 4,
            numberOfBeds: 3,
            numberOfGuests: 4,
            numberOfBathrooms: 4,
            pricePerNight: 460,
            latitude: 34.1,
            longitude: -118.1426,
            imageUrls: ["listing-5", "listing-6", "listing-7", "listing-8"],
            address: "124 Main St",
            city: "Los Angeles",
            state: "California",
            title: "Beatiful Los Angeles home in the Hollywood Hills",
            rating: 4.2,
            features: [.selfCheckIn, .superHost],
            ameneties: [.wifi, .alarmSystem, .balcony],
            type: .apartment
        )
    ]
    
    //let user = User(accountType: .guest, fullname: "John Doe", email: "johndoe@gmail.com", profileImageUrl: "male-profile-photo")
}
