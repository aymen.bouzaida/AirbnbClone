//
//  AirbnbCloneApp.swift
//  AirbnbClone
//
//  Created by Aymen Bouzaida on 30/05/2024.
//

import SwiftUI

@main
struct AirbnbCloneApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
